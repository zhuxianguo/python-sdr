
from rtlsdr import RtlSdr 
import sounddevice as sd
from sdr import *

sample_rate=240_000
sdr = RtlSdr()
sdr.sample_rate = sample_rate
sdr.center_freq = 88.7e6
sdr.gain = 'auto'

def callback(outdata, frames, time, status):
    samples=sdr.read_samples(frames*5)
    angle_data=fm_demodulation(samples)
    ldata=lfilter_data(angle_data,sample_rate)
    audio_data=resample_poly(ldata,1,5)
    reshape_data=audio_data.reshape(-1,1)
    outdata[:] = reshape_data
        
with sd.OutputStream(device=None, channels=1, callback=callback,samplerate=48_000):
    input()